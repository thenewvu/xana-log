'use strict'

const debug = require('debug')

module.exports = function (namespace) {
  const info = debug(`${namespace}:info`)
  info.log = console.info.bind(console)

  const warn = debug(`${namespace}:warn`)
  warn.log = console.warn.bind(console)

  const error = debug(`${namespace}:error`)
  error.log = (...args) => {
    console.info(...args)
    console.error(...args)
  }

  return {
    info,
    error,
    warn
  }
}
